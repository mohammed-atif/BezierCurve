#include<jni.h>
#include<stdafx.h>
#include<string>
#include<vector>
#define BLEND(RETURN_TYPE, NAME, ...) \
    extern "C" { \
    JNIEXPORT RETURN_TYPE \
    JNICALL \
    Java_com_mohammedatif_android_beziercurves_MainActivity_ ## NAME \
        (JNIEnv *env, jobject, ##__VA_ARGS__);\
    }; \
    JNIEXPORT RETURN_TYPE \
    JNICALL \
    Java_com_mohammedatif_android_beziercurves_MainActivity_ ## NAME \
        (JNIEnv *env, jobject, ##__VA_ARGS__)\
