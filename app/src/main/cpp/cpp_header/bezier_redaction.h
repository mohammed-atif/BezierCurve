//
// Created by zemoso on 18/1/18.
//

#define REDACT(RETURN_TYPE, NAME, ...) \
    extern "C" { \
    JNIEXPORT RETURN_TYPE \
    JNICALL \
    Java_com_mohammedatif_android_beziercurves_MainActivity_ ## NAME \
        (JNIEnv *env, jobject, ##__VA_ARGS__);\
    }; \
    JNIEXPORT RETURN_TYPE \
    JNICALL \
    Java_com_mohammedatif_android_beziercurves_MainActivity_ ## NAME \
        (JNIEnv *env, jobject, ##__VA_ARGS__)\
