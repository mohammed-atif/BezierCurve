#include "../cpp_header/interpolation.h"

BLEND(jfloat, blend, jint iterations) {
    float blendValue = 0;
    int start = 0;
    int end = iterations;
    for(int i=0; i<iterations; i++){
        int diff = end - start;
        int progress = i - start;
        float pc = (float)progress/(float)diff;
        pc = pc * pc * (3.0f - 2.0f * pc);
        blendValue = blendValue + pc;
    }
    return blendValue;
}

FrameData * jniPosRec = NULL;

typedef struct FrameData {
    float scale;
    float rotate;
    float panX;
    float panY;
    float saturation;
    float brightness;
};

BLEND(jobject, generateInterpolatedFrames, jclass cls){
    std::vector<FrameData*> frameData;
    FrameData *frameData1 = new FrameData();
    frameData1->scale = 3.0;
    frameData1->rotate = 67;
    frameData1->panX = 2;
    frameData1->panY = 1.4;
    frameData1->saturation = 2.8;
    frameData1->brightness = 1.6;
    FrameData *frameData2 = new FrameData();
    frameData2->scale = 1.0;
    frameData2->rotate = 77;
    frameData2->panX = 1.2;
    frameData2->panY = 1.7;
    frameData2->saturation = 0.8;
    frameData2->brightness = 0.6;
    frameData.push_back(frameData1);
    frameData.push_back(frameData2);

}

void LoadJniPosRec(JNIEnv * env) {

    if (jniPosRec != NULL)
        return;

    jniPosRec = new JNI_POSREC;

    jniPosRec->cls = env->FindClass("com/test/StudentRecord");

    if(jniPosRec->cls != NULL)
        printf("sucessfully created class");

    jniPosRec->constructortorID = env->GetMethodID(jniPosRec->cls, "<init>", "()V");
    if(jniPosRec->constructortorID != NULL){
        printf("sucessfully created ctorID");
    }

    jniPosRec->nameID = env->GetFieldID(jniPosRec->cls, "name", "Ljava/lang/String;");
    jniPosRec->rollNumberID = env->GetFieldID(jniPosRec->cls, "rollNumber", "I");
    jniPosRec->departementID = env->GetFieldID(jniPosRec->cls, "departement", "Ljava/lang/String;");
    jniPosRec->totalMarkID = env->GetFieldID(jniPosRec->cls, "totalMark", "F");
    jniPosRec->hasReservationID = env->GetFieldID(jniPosRec->cls, "hasReservation", "Z");

}