#include<jni.h>
#include<string>
#include "../cpp_header/bezier_redaction.h"

REDACT(jstring, getRandomString, jobject, jboolean isCpp) {
    std::string hello;
    if(isCpp){
        hello = "Hello from C++";
    }else {
        hello = "Hello from C";
    }
    return env->NewStringUTF(hello.c_str());
}