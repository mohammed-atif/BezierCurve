package com.mohammedatif.android.beziercurves

/**
 * Created by zemoso on 22/1/18.
 */
class FrameData {
    var scale : Float = 1.0f
    var rotate : Float = 0f
    var panX : Float = 1f
    var panY : Float = 1f
    var saturation : Float = 0f
    val brightness : Float = 1f
}