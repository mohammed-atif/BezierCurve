package com.mohammedatif.android.beziercurves

import android.app.Application
import android.util.Log

/**
 * Created by zemoso on 17/1/18.
 */
class BezierApplication : Application(){

    companion object {
        init {
            Log.i("BezierApplication", "Library loaded")
            System.loadLibrary("bezier-redact.lib")
            System.loadLibrary("interpolation.lib")
        }
    }
}