package com.mohammedatif.android.beziercurves

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.DisplayMetrics
import android.util.Log
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private var isRedacting : Boolean = false
    private val TAG : String = "MainActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val myString = getRandomString(false)
        myString?.length
        Toast.makeText(this.applicationContext, "Read the string $myString ${myString?.length}", Toast.LENGTH_SHORT).show()
    }

    override fun onResume() {
        super.onResume()
        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        val height = displayMetrics.heightPixels
        val width = displayMetrics.widthPixels
        bezier_view.setOnClickListener({
            if(!isRedacting) {
                bezier_view.startRedaction(width, height)
            }else{
                bezier_view.stopRedaction()
            }
            isRedacting = !isRedacting
        })
        var startTime : Long = System.nanoTime()
        Log.i(TAG, "JAVA Value : "+getBlendFactor(2000))
        var endTime : Long = System.nanoTime()
        Log.e(TAG, "JAVA Time : ${endTime - startTime}")
        startTime = System.nanoTime()
        Log.i(TAG, "JNI Value : ${blend(2000)}")
        endTime = System.nanoTime()
        Log.e(TAG, "JNI Time : ${endTime - startTime}")
        generateFrames.setOnClickListener({ view ->

        })
    }

    private fun getBlendFactor(iterations: Int): Float {
        var blendValue = 0f
        val start = 0
        val end = iterations
        for(i in 0 until iterations) {
            val diff = end - start
            val progress = i - start
            var pc : Float = progress.toFloat() / diff.toFloat()
            pc = pc * pc * (3.0f - 2.0f * pc)
            blendValue = blendValue + pc
        }
        return blendValue
    }

    private external fun getRandomString(isCpp : Boolean) : String?
    private external fun blend(iterations : Int) : Float
    private external fun generateInterpolatedFrames() : Array<FrameData>
}
