package com.mohammedatif.android.beziercurves

import android.content.Context
import android.graphics.*
import android.os.Handler
import android.os.HandlerThread
import android.support.v7.widget.AppCompatImageView
import android.util.AttributeSet
import android.util.Log
import java.util.*


/**
 * author atif
 * Created on 15/01/18.
 */

class BezierSample : AppCompatImageView {

    private val TAG : String = "BazierSample"
    private val REDACT_THREAD_ID = "RedactThread"
    private val REDACT_TIMER : Long = 100
    private val CIRCLE_COUNT : Int = 50
    private val MAX_RADIUS : Int = 50
    private val MIN_RADIUS : Int = 10
    private val randomNumberGenerator : Random = Random()
    private val colorArray : IntArray = intArrayOf(
            Color.GRAY,
            Color.BLACK,
            Color.BLUE,
            Color.CYAN,
            Color.DKGRAY,
            Color.GREEN,
            Color.MAGENTA,
            Color.RED,
            Color.YELLOW
    )

    private val paint : Paint = Paint()
    private val path : Path = Path()

    private var srcRect : Rect? = null
    private var destRect : Rect? = null

    private var redactCanvas: Canvas? = null
    private var redactBitmap : Bitmap? = null
    private var colorArraySize : Int = 0

    private var screenWidth : Int? = null
    private var screenHeight : Int? = null

    private var redactHandler : Handler? = null
    private var redactionThread : HandlerThread? = null
    private var redactRunnable : Runnable? = null

    //region Constructors

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attr: AttributeSet) : super(context, attr) {
        init()
    }

    constructor(context: Context, attr: AttributeSet, defStyleAttr: Int) : super(context, attr, defStyleAttr) {
        init()
    }

    //endregion

    //region Private Methods

    private fun init(){
        colorArraySize = colorArray.size
    }

    //endregion

    //region Overridden Methods

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        redactBitmap?.let {
            canvas?.drawBitmap(redactBitmap, srcRect, destRect, null)
        }
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        stopRedaction()
    }

    //endregion

    //region Private Methods

    fun startRedaction(width : Int, height : Int){
        Log.i(TAG, "redaction started with screen width $width and height $height")
        this.screenWidth = width/4
        this.screenHeight = height/4
        this.srcRect = Rect(0, 0, this.screenWidth!!, this.screenHeight!!)
        this.destRect = Rect(0, 0, width, height)
        redactionThread = HandlerThread(REDACT_THREAD_ID, Thread.MAX_PRIORITY)
        redactionThread!!.start()
        redactHandler = Handler(redactionThread!!.looper)
        redactRunnable = Runnable {
            redact()
            redactHandler?.postDelayed(redactRunnable, REDACT_TIMER)
        }
        redactHandler?.post(redactRunnable)
    }

    fun stopRedaction(){
        redactHandler?.removeCallbacks(redactRunnable)
        redactionThread?.quitSafely()
        redactCanvas?.setBitmap(null)
        Log.d(TAG, "total memory allocated to the non recycled bitmap "+redactBitmap?.allocationByteCount)
        redactBitmap?.recycle()
        Log.d(TAG, "total memory allocated to the recycled bitmap "+redactBitmap?.allocationByteCount)
        path.reset()
        redactCanvas = null
        redactRunnable = null
        redactHandler = null
        redactionThread = null
        redactBitmap = null
        postInvalidate()
        System.gc()
    }

    private fun redact(){
        redactBitmap?.eraseColor(Color.TRANSPARENT)
        if(redactBitmap == null) {
            redactBitmap = Bitmap.createBitmap(screenWidth!!, screenHeight!!, Bitmap.Config.ARGB_8888)
        }
        if(redactCanvas == null){
            redactCanvas = Canvas(redactBitmap)
        }
        redactCanvas?.setBitmap(redactBitmap)
        for(i in 0 until CIRCLE_COUNT){
            path.reset()
            plotRedactionPoint(redactCanvas!!, colorArray[randomNumberGenerator.nextInt(colorArraySize)])
        }
        redactCanvas?.setBitmap(null)
        postInvalidate()
    }

    private fun plotRedactionPoint(redactCanvas : Canvas, color : Int){
        paint.style = Paint.Style.FILL
        paint.color = color
        paint.alpha = 200
        var randomStartX : Int = randomNumberGenerator.nextInt(screenWidth!!-MAX_RADIUS)+MIN_RADIUS
        var randomStartY : Int = randomNumberGenerator.nextInt(screenHeight!!-MAX_RADIUS)+MIN_RADIUS
        val radius : Int = randomNumberGenerator.nextInt(MAX_RADIUS-MIN_RADIUS)+MIN_RADIUS
        randomStartX +=radius
        randomStartY +=radius
        if(color % 2 == 0) {
            path.addCircle(randomStartX.toFloat(), randomStartY.toFloat(), radius.toFloat(), Path.Direction.CCW)
        }else{
            path.addRoundRect(randomStartX.toFloat(), randomStartY.toFloat(), (randomStartX+radius).toFloat(), (randomStartY+radius).toFloat(), 5f, 5f, Path.Direction.CCW)
        }
        redactCanvas.drawPath(path, paint)
    }

    //endregion

}
